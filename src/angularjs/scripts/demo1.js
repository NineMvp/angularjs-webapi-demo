﻿// main.js
var app = angular.module('myApp', ['ngGrid']);

app.controller('MyCtrl', function ($scope) {

    $scope.myData = [];
    var removeTemplate = '<input type="button" value="remove" ng-click="removeRow(row)" />';

    $scope.gridOptions = {
        data: 'myData',
        showGroupPanel: true,
        enableCellSelection: true,
        enableRowSelection: false,
        enableCellEdit: true,
        showSelectionCheckbox: true,
        columnDefs: [{ field: 'name', displayName: 'Name' }, { field: 'age', displayName: 'Age' }, { field: 'remove', displayName: 'Remove', cellTemplate: removeTemplate }]
    };
    
    $scope.name = "";
    $scope.age = 0;

    $scope.addRow = function () {
        $scope.myData.push({ name: $scope.name, age: $scope.age });
    };
    
    $scope.removeRow = function (row) {
        $scope.myData.splice(row.rowIndex, 1);
    };
    
    $scope.saveData = function() {
        if ($scope.myData.length === 0) {
            alert('No data');
        } else {
            alert('Save completed! ' + JSON.stringify($scope.myData));
        }
    };
});
