﻿// main.js
var app = angular.module('demo2', []);

app.controller('demo2Ctrl', function ($scope) {
    $scope.name = '';
    $scope.capitalise = function () {
        var text = $scope.name, split = text.split(" "), res = [], i, len, component;
        for (i = 0, len = split.length; i < len; i++) {
            component = split[i];
            res.push(component.substring(0, 1).toUpperCase());
            res.push(component.substring(1));
            res.push(" "); // put space back in
        }
        return res.join("");
    };
});

app.controller('demo2SubCtlr', function ($scope) {
    $scope.name2 = function () { return $scope.name; };
    $scope.age = 0;
});

app.controller('demo2SubOfSubCtlr', function($scope) {
    $scope.name21 = function() { return $scope.name2(); };
    $scope.hidden = function() {
        return angular.element("#hidden").val();
    };
});
