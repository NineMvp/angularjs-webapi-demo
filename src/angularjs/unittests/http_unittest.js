﻿/// <reference path="../scripts/lib/jasmine-1.3.0/jasmine.js" />
/// <reference path="../scripts/angular.min.js" />

escribe('myApp', function() {
    // load the relevant application modules then load a special
    // test module which overrides the $window with a mock version,
    // so that calling window.alert() will not block the test
    // runner with a real alert box. This is an example of overriding
    // configuration information in tests.
    beforeEach(module('greetMod', function($provide) {
        $provide.value('$window', {
            alert: jasmine.createSpy('alert')
        });
    }));

    it('should make an xhr GET request', function() {
        element(':button:contains("Sample GET")').click();
        element(':button:contains("fetch")').click();
        expect(binding('status')).toBe('200');
        expect(binding('data')).toMatch(/Hello, \$http!/);
    });

    it('should make a JSONP request to angularjs.org', function() {
        element(':button:contains("Sample JSONP")').click();
        element(':button:contains("fetch")').click();
        expect(binding('status')).toBe('200');
        expect(binding('data')).toMatch(/Super Hero!/);
    });

    it('should make JSONP request to invalid URL and invoke the error handler',
        function() {
            element(':button:contains("Invalid JSONP")').click();
            element(':button:contains("fetch")').click();
            expect(binding('status')).toBe('0');
            expect(binding('data')).toBe('Request failed');
        });

});