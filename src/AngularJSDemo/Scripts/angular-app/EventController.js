﻿"use strict";



eventsApp.controller('EventController', function EventController($scope, $log, eventservice) {

    var map, point, lastCenter, lastZoom;

    $scope.searchText = '';
    
    $scope.events = [];

    $scope.event = null; 
    //{
    //    name: 'Zubzib Black Coffee #9',
    //    subtitle: 'Skills Update v2',
    //    date: '24/11/2013',
    //    time: '09:00 am',
    //    location: {
    //        address1: 'The University of the Thai Chamber of Commerce(UTCC)',
    //        address2: '126/1 Vibhavadee-Rangsit Road, Din Daeng',
    //        province: 'Bangkok',
    //        country: 'Thailand',
    //        map: {
    //            latitude: 13.779631,
    //            longitude: 100.560991
    //        }
    //    },
    //    eventPhotoUrl: '../../Content/AngularJS-large.png',
    //    sessions: [
    //        {
    //            name: 'Create Web App with AngularJS and Web APIs.',
    //            instructor: 'Chalermpon Areepong',
    //            duration: '120 minnutes',
    //            level: 'intermediate',
    //            detail: 'Introduce to use AngularJS to build your web application and connecting to ASP.NET Web APIs.'
    //        },
    //        {
    //            name: 'ASP.NET Identity and OWIN.',
    //            instructor: 'Non Intanon',
    //            duration: '50 minnutes',
    //            level: 'beginner',
    //            detail: 'N/A'
    //        },
    //        {
    //            name: 'Unit Test with NUnit.',
    //            instructor: 'Theeranit Amp',
    //            duration: '50 minnutes',
    //            level: 'beginner',
    //            detail: 'N/A'
    //        },
    //        {
    //            name: 'SignalR.',
    //            instructor: 'SuperTee suudsuud',
    //            duration: '50 minnutes',
    //            level: 'beginner',
    //            detail: 'N/A'
    //        }
    //    ]
    //};

    function renderMap() {

        if (map != null || map != undefined)
            map.destroy();
        point = new google.maps.LatLng($scope.event.location.map.latitude, $scope.event.location.map.longitude);
        
        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 16,
            center: point
        });

        var marker = new google.maps.Marker({ position: point, map: map });

        lastCenter = map.getCenter();
        lastZoom = map.getZoom();
        google.maps.event.trigger(map, 'resize');
        map.setCenter(lastCenter);
        map.setZoom(lastZoom);

    }

    $scope.showMap = function () {
        $('#modal').modal('show');
        renderMap();
    };

    $scope.searchEvents = function () {
        $log.log(eventservice);
        eventservice.searchEvents($scope.searchText).then(function (response) {
            console.log('search');
            $scope.events = response.data;
            console.log($scope.events);
            if ($scope.events.length > 0) {
                //$scope.event = $scope.events[0];
                //console.log($scope.event);
            } else {
                if (map != null || map != undefined)
                    map.destroy();
                $('#map').html('<h3>Not found!</h3>');
                $('#modal').modal('show');
            }
        });
    };

    $scope.showDetail = function(index) {
        $scope.event = $scope.events[index];
    };

});