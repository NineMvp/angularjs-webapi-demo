﻿"use strict";

var eventsApp = angular.module('eventsApp', ['ngAnimate']);

eventsApp.service('eventservice', ['$http',
    function ($http) {
        //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $http.defaults.headers.post["Content-Type"] = "application/json";

        this.getEvents = function () {
            var promise = $http({
                method: 'GET',
                url: '/api/events'
            }).success(function (data, status, headers, config) {
                console.log('Done');
                console.log(data);
                var events = data;
                return events;
            });

            return promise;
        };
        
        this.getEvent = function (eventId) {
            var promise = $http({
                method: 'GET',
                url: '/api/events/' + eventId
            }).success(function (data, status, headers, config) {
                console.log('Done');
                console.log(data);
                var events = data;
                return events;
            });

            return promise;
        };
        
        this.searchEvents = function (searchText) {
            var promise = $http({
                method: 'GET',
                url: '/api/events/searchevents/' + escape(searchText)
            }).success(function (data, status, headers, config) {
                console.log('Done');
                console.log(data);
                var events = data;
                return events;
            });
            return promise;
        };
        
        this.addEvent = function(event) {
            var promise = $http({
                method: 'POST',
                url: '/api/events/',
                data: event
            }).success(function (data, status, headers, config) {
                console.log('Done');
                console.log(data);
                var events = data;
                return events;
            });
            return promise;
        }
    }]);



