﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJSDemo.Models
{
    public class Event
    {
        public string EventId { get; set; }
        public string Name { get; set; }
        public string SubTitle { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string Contact { get; set; }
        public string EventPhotoUrl { get; set; }
        public Location Location { get; set; }
        public List<Session> Sessions { get; set; }
    }

    public class Location
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public Map Map { get; set; }
    }

    public class Map
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class Session
    {
        public string SessionId { get; set; }
        public string Name { get; set; }
        public string Instructor { get; set; }
        public string Duration { get; set; }
        public Level Level { get; set; }
        public string Details { get; set; }
    }

    public enum Level
    {
        Intermediate, Advanced, Beginner, Monkey
    }
}
