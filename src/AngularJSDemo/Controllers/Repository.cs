﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Web;
using AngularJSDemo.Models;

namespace AngularJSDemo.Controllers
{
    public static class Repository
    {
        public static List<Event> Events = new List<Event>
        {
            new Event
            {
                EventId = "ZZBC#09",
                Name = "Zubzib Black Coffee #9",
                SubTitle = "Skills Update v2",
                Date = new DateTime(2013, 11, 24),
                Time = "09:00 am",
                Contact = "(082) 074-7776 - Mr.Nine",
                EventPhotoUrl = "../../Content/AngularJS-large.png",
                Location = new Location
                {
                    Address1 = "The University of the Thai Chamber of Commerce(UTCC)",
                    Address2 = "126/1 Vibhavadee-Rangsit Road, Din Daeng",
                    Province = "Bangkok",
                    Country = "Thailand",
                    Map = new Map {Latitude = 13.779631, Longitude = 100.560991}
                },
                Sessions = new List<Session>
                {
                    new Session
                    {
                        SessionId = "S001",
                        Name = "Create Web App with AngularJS and Web APIs.",
                        Instructor = "Chalermpon Areepong",
                        Duration = "120 minnutes",
                        Level = Level.Intermediate,
                        Details = "Introduce to use AngularJS to build your web application and connecting to ASP.NET Web APIs."
                    },
                    new Session
                    {
                        SessionId = "S002",
                        Name = "ASP.NET Identity and OWIN",
                        Level = Level.Intermediate,
                        Instructor = "Non Intanon",
                        Duration = "60 Minutes",
                        Details = "N/A"
                    },
                    new Session
                    {
                        SessionId = "S003",
                        Name = "Unit Testing with NUnit",
                        Level = Level.Intermediate,
                        Instructor = "Theeranit Amp",
                        Duration = "60 Minutes",
                        Details = "N/A"
                    },
                    new Session
                    {
                        SessionId = "S004",
                        Name = "SignalR",
                        Level = Level.Intermediate,
                        Instructor = "Supertee SuudSuud",
                        Duration = "60 Minutes",
                        Details = "N/A"
                    }
                }

            }
        };

        public static IEnumerable<Event> Get()
        {
            return Events;
        }

        public static Event Get(string eventId)
        {
            return Events.SingleOrDefault(o => o.EventId == eventId);
        }

        public static bool Add(Event @event)
        {
            if (!Events.Exists(o => o.EventId == @event.EventId))
            {
                Events.Add(@event);
                return true;
            }
            return false;
        }

        public static bool Update(Event @event)
        {
            if (Events.Exists(o => o.EventId == @event.EventId))
            {
                Events.Remove(Events.Find(s => s.EventId == @event.EventId));
                Events.Add(@event);
                return true;
            }
            return false;
        }

        public static bool Remove(string eventId)
        {
            if (Events.Exists(o => o.EventId == eventId))
            {
                Events.Remove(Events.Find(s => s.EventId == eventId));
                return true;
            }
            return false;
        }

        public static bool Add(string eventId, Session session)
        {
            if (Events.Exists(o => o.EventId == eventId))
            {
                Events.Single(o => o.EventId == eventId).Sessions.Add(session);
                return true;
            }
            return false;
        }

        public static bool Update(string eventId, Session session)
        {
            if (Events.Exists(o => o.EventId == eventId))
            {
                var @event = Events.Single(o => o.EventId == eventId);
                @event.Sessions.Remove(@event.Sessions.Find(s => s.SessionId == session.SessionId));
                return true;
            }
            return false;
        }

        public static bool Remove(string eventId, string sessionId)
        {
            var eventResult = Events.SingleOrDefault(o => o.EventId == eventId);
            if (eventResult != null && eventResult.Sessions.Exists(s => s.SessionId == sessionId))
            {
                eventResult.Sessions.Remove(eventResult.Sessions.Find(s => s.SessionId == sessionId));
                return true;
            }
            return false;
        }

        public static IEnumerable<Event> Find(string searchText)
        {
            return Events.Where(o =>
                o.Location.Address1.Contains(searchText) || o.Location.Address2.Contains(searchText) ||
                o.Location.Country.Contains(searchText) || o.Location.Province.Contains(searchText) ||
                o.Name.Contains(searchText) || o.SubTitle.Contains(searchText) ||
                o.Date.ToString("dd/MM/yyyy").Contains(searchText));
        }

        public static IEnumerable<Session> Find(string eventId, string searchText)
        {
            var eventResult = Events.SingleOrDefault(o => o.EventId == eventId);
            if (eventResult == null)
                return null;
            return eventResult.Sessions.Where(s =>
                s.Duration.Contains(searchText) || s.Name.Contains(searchText) ||
                s.Instructor.Contains(searchText) || s.SessionId.Contains(searchText) ||
                s.Level.ToString().Contains(searchText));
        }

    }
}