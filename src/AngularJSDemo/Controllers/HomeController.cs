﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularJSDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AngularJSDemo1()
        {
            return View();
        }

        public ActionResult AngularJSDemo2()
        {
            return View();
        }

        public ActionResult AngularJSDemo3()
        {
            return View();
        }

        public ActionResult AngularJSDemo4()
        {
            return View();
        }

        public ActionResult AngularJSDemo5()
        {
            return View();
        }

    }
}