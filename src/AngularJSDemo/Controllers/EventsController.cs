﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSDemo.Models;

namespace AngularJSDemo.Controllers
{
    [RoutePrefix("api/events")]
    public class EventsController : ApiController
    {
        [Route(""), HttpGet]
        public HttpResponseMessage GetEvent(HttpRequestMessage request)
        {
            return request.CreateResponse(HttpStatusCode.Found, Repository.Get());
        }

        [Route("{eventId:alpha}"), HttpGet]
        public HttpResponseMessage GetEventById(HttpRequestMessage request, string eventId)
        {
            var res = Repository.Get(eventId);
            return request.CreateResponse(res != null ? HttpStatusCode.OK : HttpStatusCode.NotFound, res);
        }

        [Route("searchevents"), Route("searchevents/{searchText:alpha}"), HttpGet]
        public HttpResponseMessage SearchEvents(HttpRequestMessage request, string searchText)
        {var res = Repository.Find(searchText);
            return request.CreateResponse(res != null ? HttpStatusCode.OK : HttpStatusCode.NotFound, res);
        }

        [Route("searchsessions"), Route("searchsessions/{eventId:alpha}/{searchText:alpha}"), HttpGet]
        public HttpResponseMessage SearchSesssion(HttpRequestMessage request, string eventId, string searchText)
        {
            var res = Repository.Find(eventId, searchText);
            return request.CreateResponse(res != null ? HttpStatusCode.OK : HttpStatusCode.NotFound, res);
        }

        [Route(""), HttpPost]
        public HttpResponseMessage AddEvent(HttpRequestMessage request, Event @event)
        {
            return request.CreateResponse(Repository.Add(@event) ? HttpStatusCode.OK : HttpStatusCode.NotAcceptable);
        }

        [Route(""), HttpPut]
        public HttpResponseMessage UpdateEvent(HttpRequestMessage request, Event @event)
        {
            return request.CreateResponse(Repository.Update(@event) ? HttpStatusCode.OK : HttpStatusCode.NotAcceptable);
        }

        [Route("{eventId:alpha}"), HttpDelete]
        public HttpResponseMessage RemoveEventById(HttpRequestMessage request, string eventId)
        {
            return request.CreateResponse(Repository.Remove(eventId) ? HttpStatusCode.OK : HttpStatusCode.NotAcceptable);
        }

        [Route("sessions"), Route("sessions/{eventId:alpha}"), HttpPost]
        public HttpResponseMessage AddSession(HttpRequestMessage request, string eventId, Session session)
        {
            return
                request.CreateResponse(Repository.Add(eventId, session)
                    ? HttpStatusCode.OK
                    : HttpStatusCode.NotAcceptable);
        }

        [Route("sessions"), Route("sessions/{eventId:alpha}"), HttpPut]
        public HttpResponseMessage UpdateSession(HttpRequestMessage request, string eventId, Session session)
        {
            return
                request.CreateResponse(Repository.Update(eventId, session)
                    ? HttpStatusCode.OK
                    : HttpStatusCode.NotAcceptable);
        }

        [Route("sessions"),Route("sessions/{eventId:alpha}/{sessionId:alpha}"), HttpDelete]
        public HttpResponseMessage RemoveSession(HttpRequestMessage request, string eventId, string sessionId)
        {
            return
                request.CreateResponse(Repository.Remove(eventId, sessionId)
                    ? HttpStatusCode.OK
                    : HttpStatusCode.NotAcceptable);
        }
    }
}
