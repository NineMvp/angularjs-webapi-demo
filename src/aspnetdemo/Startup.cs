﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(aspnetdemo.Startup))]
namespace aspnetdemo
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
